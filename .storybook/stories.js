import React from 'react';
import { storiesOf, setAddon } from '@storybook/react';
// import JSXAddon from 'storybook-addon-jsx';


// setAddon(JSXAddon);



const divListItems = [
  { id: 0, text: "Fish fingers and custard" },
  { id: 1, text: "Alonze!" },
  { id: 2, text: "42" }
];



storiesOf("CSS Class Samples", module)
  .add("__sb-code__", () => (
    <span className="__sb-code__">
      Nulla laborum eiusmod eius quaerat pellentesque morbi nemo, senectus quo at nascetur sapiente pellentesque debitis.
    </span>
  ))
  .add("__sb-div-list__", () => (
    <div className="__sb-div-list__">
      {divListItems.map((item) => (
        <div key={item.id}>
          {item.text}
        </div>
      ))}
    </div>
  ))
  .add("__sb-grid__", () => (
    <div className="__sb-grid__" style={{ gridTemplateColumns: 'max-content auto' }}>
      {divListItems.map((item) => [
        <label className="__sb-grid-label-cell__">Item {item.id}{item.id === 1 ? " (Extra Text To Extend Column Width)" : undefined}</label>,
        <div>{item.text}</div>
      ])}
    </div>
  ))
  .add("__sb-sections-wrapper__", () => (
    <div className="__sb-sections-wrapper__">

      <section>
        <h3>Section One</h3>
        <div>
          Suscipit necessitatibus soluta molestias eum duis incidunt? Tempore aenean suscipit.
          Diam rerum, voluptatum reiciendis tristique necessitatibus? Explicabo iste praesentium
          dicta proin! Tempore. Assumenda saepe! Per nihil natoque! Curae? Lacinia necessitatibus,
          pede illo nisl nostra nesciunt tempus, cupidatat purus cubilia, laborum! Voluptas
          mauris ducimus netus unde. Erat repellat est esse ipsa.
        </div>
      </section>

      <section>
        <h3>Section Two</h3>
        <div>
          Libero nam voluptas arcu lacus modi potenti cupidatat fames placeat velit doloribus
          lectus accusamus, dapibus placeat. Accumsan congue rem? Tempus, aenean excepteur
          adipisicing minima, convallis malesuada totam eveniet est porttitor, condimentum
          bibendum? Eum tortor alias, pellentesque magni odio consequuntur sociosqu? Maxime dolores
          numquam minim eligendi, quo, laudantium vivamus, voluptate cubilia.
        </div>
      </section>

    </div>
  ))
  .add("__sb-table__", () => (
    <table className="__sb-table__">
      <thead>
        <tr>
          <th>Icon</th>
          <th>Name</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td><i class="fa fa-calendar" aria-hidden="true"></i></td>
          <td>calendar</td>
        </tr>
        <tr>
          <td><i class="fa fa-caret-up" aria-hidden="true"></i></td>
          <td>caret-up</td>
        </tr>
        <tr>
          <td><i class="fa fa-envelope-o" aria-hidden="true"></i></td>
          <td>envelope-o</td>
        </tr>
        <tr>
          <td><i class="fa fa-search" aria-hidden="true"></i></td>
          <td>search</td>
        </tr>
        <tr>
          <td><i class="fa fa-pencil-square-o" aria-hidden="true"></i></td>
          <td>pencil-square-o</td>
        </tr>
        <tr>
          <td><i class="fa fa-print" aria-hidden="true"></i></td>
          <td>print</td>
        </tr>
      </tbody>
    </table>
  ));
