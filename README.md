# storybook-preview-head

[![ISC License](https://img.shields.io/badge/license-ISC-blue.svg?style=flat-square)][license]

> Resources usable in storybook and intended to be imported via `preview-head.html` (see
[storybook docs](https://storybook.js.org/configurations/add-custom-head-tags/) for details).


[View Samples Online][samples]


<br/>
<br/>


# Usage

**preview-head.html**

```html
<link rel="stylesheet" type="text/css" href="https://bsara.gitlab.io/storybook-preview-head/styles.min.css">
```

<br/>


## Automatically Applied Styles

There is really only one noticable style that is automatically applied when using `styles.css`
in your storybook implementation: An angled grid background. The purpose of this background is
to show which elements/components have a background set and those which do not. The grid is
angled at 45 degrees is to reduce the possiblity that the grid lines will blend with borders
or other styles you've applied to your elements/components, thus allowing for a much more
clear distinction between background and element/component. (For an example)

<br/>

## CSS Classes

### \_\_sb-code\_\_

Used for elements whose contents are meant to be displayed as "code".

**Example ([View Online][sb-code-sample]):**

```html
<span className="__sb-code__">{/* Put anything here... */}</span>
```

<br/>

### \_\_sb-div-list\_\_

Used for general lists.

[View online sample of example below][sb-div-list-sample]

**Example ([View Online][sb-div-list-sample]):**

```html
<div className="__sb-div-list__">
  {items.map((item) => (
    <div key={item.id}>
      {/* item stuff... */}
    </div>
  ))}
</div>
```

<br/>

### \_\_sb-grid\_\_

Used to display elements/components in a grid layout. You can use any CSS grid properties
to alter the layout to your liking.

**Example ([View Online][sb-grid-sample]):**

```html
<div className="__sb-grid__">
  {/* Put anything here... */}
</div>
```

<br/>

### \_\_sb-grid-label-cell\_\_

Used to format child cells of a `.__sb-grid__` element.

**Example ([View Online][sb-grid-sample]):**

```html
<div className="__sb-grid__">
  <label className="__sb-grid-label-cell__">My label</label>
  {/* Put anything here... */}
</div>
```

<br/>

### \_\_sb-sections-wrapper\_\_

Used to list multiple examples in one story, separate into sections. Margins are automatically
added to defined sections and styles are automatically added to section headers (if present).

**Example ([View Online][sb-sections-wrapper-sample]):**

```html
<div className="__sb-sections-wrapper__">

  <section>
    <h3>Section One</h3>
    {/* Section 1 stuff... */}
  </section>

  <section>
    <h3>Section Two</h3>
    {/* Section 2 stuff... */}
  </section>

</div>
```

<br/>

### \_\_sb-table\_\_

Used to display things in a table. Cell margins, font-families, and background colors are
automatically set.

**Example ([View Online][sb-table-sample])**

```html
<table className="__sb-table__">
  <thead>
    <tr>
      <th>Icon</th>
      <th>Name</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><i class="fa fa-calendar" aria-hidden="true"></i></td>
      <td>calendar</td>
    </tr>
    <tr>
      <td><i class="fa fa-caret-up" aria-hidden="true"></i></td>
      <td>caret-up</td>
    </tr>
    <tr>
      <td><i class="fa fa-envelope-o" aria-hidden="true"></i></td>
      <td>envelope-o</td>
    </tr>
    <tr>
      <td><i class="fa fa-search" aria-hidden="true"></i></td>
      <td>search</td>
    </tr>
    <tr>
      <td><i class="fa fa-pencil-square-o" aria-hidden="true"></i></td>
      <td>pencil-square-o</td>
    </tr>
    <tr>
      <td><i class="fa fa-print" aria-hidden="true"></i></td>
      <td>print</td>
    </tr>
  </tbody>
</table>
```


<br/>
<br/>


# License

ISC License (ISC)

Copyright (c) 2018, Brandon D. Sara (http://bsara.pro/)

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.



[license]: https://gitlab.com/bsara/storybook-preview-head/blob/master/LICENSE "License"
[samples]: https://bsara.gitlab.io/storybook-preview-head/samples/?selectedKind=CSS%20Class%20Samples&selectedStory=__sb-code__

[sb-code-sample]:             https://bsara.gitlab.io/storybook-preview-head/samples/?selectedKind=CSS%20Class%20Samples&selectedStory=__sb-code__
[sb-div-list-sample]:         https://bsara.gitlab.io/storybook-preview-head/samples/?selectedKind=CSS%20Class%20Samples&selectedStory=__sb-div-list__
[sb-grid-sample]:             https://bsara.gitlab.io/storybook-preview-head/samples/?selectedKind=CSS%20Class%20Samples&selectedStory=__sb-grid__
[sb-sections-wrapper-sample]: https://bsara.gitlab.io/storybook-preview-head/samples/?selectedKind=CSS%20Class%20Samples&selectedStory=__sb-sections-wrapper__
[sb-table-sample]:            https://bsara.gitlab.io/storybook-preview-head/samples/?selectedKind=CSS%20Class%20Samples&selectedStory=__sb-table__
