#!/usr/bin/env node

const fs   = require('fs');
const path = require('path');

const unified          = require('unified');
const autolinkHeadings = require('remark-autolink-headings');
const highlight        = require('remark-highlight.js');
const html             = require('remark-html');
const markdown         = require('remark-parse');
const slug             = require('remark-slug');

const generateGithubMarkdownCSS = require('generate-github-markdown-css');



const indexHtmlBody = unified().use(markdown)
                               .use(slug)
                               .use(autolinkHeadings)
                               .use(highlight)
                               .use(html)
                               .processSync(fs.readFileSync('README.md'));

generateGithubMarkdownCSS().then((css) => {
  fs.copyFileSync(path.resolve(__dirname, 'code-highlight.css'), path.resolve(__dirname, '../public/code-highlight.css'))

  fs.writeFileSync(path.resolve(__dirname, '../public/index.css'), `${css}
  .markdown-body {
    max-width: 1012px;
    padding: 0 16px;
    margin: 32px auto;
  }`);

  fs.writeFileSync(path.resolve(__dirname, '../public/index.html'),
    `<!DOCTYPE html>`
    + `<html lang="en-US">`
    + `<head>`
    + `<meta charset="UTF-8">`
    + `<meta http-equiv="X-UA-Compatible" content="IE=edge">`
    + `<meta name="viewport" content="width=device-width, initial-scale=1">`
    + `<title>Storybook Preview Head by bsara</title>`
    + `<meta name="description" content="Resources usable in storybook and intended to be imported via preview-head.html.">`
    + `<link rel="stylesheet" href="index.css">`
    + `<link rel="stylesheet" href="code-highlight.css">`
    + `</head>`
    + `<body><div class="markdown-body">${indexHtmlBody}</div></body>`
    + `</html>`
  );
});
